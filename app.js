"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require('express');
var cors = require('cors');
var http = require("http");
var WebSocket = require("ws");
var database_js_1 = require("./database.js");
var db = new database_js_1.Database();
db.initialize().then(function (value) {
    console.log(value);
    db.insert("server", "Welcome!")
        .then(function (value) { console.log(value); })
        .catch(function (error) { console.error(error); });
    var PORT = 8001;
    var app = express();
    app.use(express.json());
    var server = http.createServer(app);
    var wss = new WebSocket.Server({ server: server });
    wss.on('connection', function (ws) {
        ws.isAlive = true;
        ws.on('pong', function () { ws.isAlive = true; });
        ws.on('message', function (message) {
            try {
                var messageToJSON = JSON.parse(message);
                console.log(message);
                db.insert(messageToJSON.sender, messageToJSON.message)
                    .then(function (value) {
                    wss.clients
                        .forEach(function (client) {
                        if (client != ws) {
                            client.send(message);
                        }
                    });
                })
                    .catch(function (error) { throw error; });
            }
            catch (e) {
                console.error(e);
                ws.send(JSON.stringify({ sender: "server", message: "Couldn't read message. You sent: " + message }));
            }
        });
        db.getAll()
            .then(function (rows) {
            for (var i = 0; i < rows.length; i++) {
                ws.send(JSON.stringify({ sender: rows[i].sender, message: rows[i].content }));
            }
        })
            .catch(function (error) { return console.log(error); });
    });
    setInterval(function () {
        wss.clients.forEach(function (ws) {
            if (!ws.isAlive)
                return ws.terminate();
            ws.isAlive = false;
            ws.ping(null, false, true);
        });
    }, 10000);
    var corsOptions = {
        origin: 'http://localhost:3000',
        optionsSuccessStatus: 200,
    };
    app.use(cors(corsOptions));
    app.get('/', function (req, res, next) {
        res.send("Hello World!");
    });
    server.listen(PORT, function () {
        console.log("Server is listening on port " + PORT + "...");
    });
}).catch(function (error) { console.error(error); });

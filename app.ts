const express = require('express');
const cors = require('cors');
import * as http from 'http';
import * as WebSocket from 'ws';

import { Database } from './database.js';

const db = new Database();
db.initialize().then(value => {
  console.log(value);
  db.insert("server", "Welcome!")
    .then(value => {console.log(value)})
    .catch(error => {console.error(error)});

  const PORT = 8001;

  const app = express();
  app.use(express.json());

  const server = http.createServer(app);

  const wss = new WebSocket.Server({ server });

  wss.on('connection', (ws: WebSocket) => {

    ws.isAlive = true;
    ws.on('pong', () => {ws.isAlive = true});

    ws.on('message', (message: string) => {
      try {
        const messageToJSON = JSON.parse(message);
        console.log(message);
        db.insert(messageToJSON.sender, messageToJSON.message)
          .then(value => {
            wss.clients
              .forEach(client => {
                if (client != ws) {
                  client.send(message);
                }
              })
          })
          .catch(error => {throw error})
      }
      catch (e) {
        console.error(e);
        ws.send(JSON.stringify({sender: "server", message: `Couldn't read message. You sent: ${message}`}));
      }

    });
    db.getAll()
      .then(rows => {
        for(let i = 0; i < rows.length; i++) {
          ws.send(JSON.stringify({sender: rows[i].sender, message: rows[i].content}));
        }
      })
      .catch(error => console.log(error));
  })

  setInterval(() => {
    wss.clients.forEach((ws: WebSocket) => {
      if (!ws.isAlive) return ws.terminate();

      ws.isAlive = false;
      ws.ping(null, false, true);
    });
  }, 10000)


  const corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200,
  }

  app.use(cors(corsOptions));

  app.get('/', (req, res, next) => {
    res.send("Hello World!");
  })

  server.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}...`);
  })

}
).catch(error => {console.error(error)})

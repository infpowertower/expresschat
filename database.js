"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var sqlite = require("sqlite3");
var Database = /** @class */ (function () {
    function Database() {
        this.db = new sqlite.Database(':memory:');
    }
    Database.prototype.initialize = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.db.run('CREATE TABLE message (sender TEXT, content TEXT);', function (error) {
                if (error) {
                    reject(error);
                }
                else {
                    resolve("Database initialized!");
                }
            });
        });
    };
    //toDo check for special characters
    Database.prototype.insert = function (sender, message) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var statement = _this.db.prepare("INSERT INTO message (sender, content) VALUES (?, ?);");
            statement.run([sender, message]);
            try {
                statement.finalize();
                resolve("Inserted new message \"" + message + "\" from \"" + sender + "\" into database.");
            }
            catch (error) {
                reject(error);
            }
        });
    };
    Database.prototype.getAll = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.db.all('SELECT * FROM message;', function (error, rows) {
                if (error)
                    reject(error);
                else
                    resolve(rows);
            });
        });
    };
    Database.prototype.close = function () {
        this.db.close();
    };
    return Database;
}());
exports.Database = Database;

import * as sqlite from 'sqlite3';

export class Database {
  db: sqlite.Database;
  constructor() {
    this.db = new sqlite.Database(':memory:');
  }

  initialize() {
    return new Promise((resolve, reject) => {
      this.db.run('CREATE TABLE message (sender TEXT, content TEXT);', (error: any) => {
        if (error) {
          reject(error);
        }
        else {
          resolve("Database initialized!");
        }
      });
    })
  }

//toDo check for special characters
  insert(sender: string, message: string) {
    return new Promise((resolve, reject) => {
      const statement = this.db.prepare("INSERT INTO message (sender, content) VALUES (?, ?);");
      statement.run([sender, message]);
      try {
        statement.finalize()
        resolve(`Inserted new message "${message}" from "${sender}" into database.`);
      }
      catch (error) {
          reject(error);
      }
    })
  }

  getAll() {
    return new Promise((resolve, reject) => {
      this.db.all('SELECT * FROM message;', (error: any, rows: []) => {
        if (error) reject(error)
        else resolve(rows);
      })
    })

  }

  close() {
    this.db.close();
  }
}
